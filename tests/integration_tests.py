#!/usr/bin/python3 -u
import nice_mock
import requests
import time
from multiprocessing import Process


def assert_response(path, code, *, some_header, source_ip='10.0.0.3'):
    headers = { 'X-Forwarded-For': source_ip, 'X-Forwarded-Proto': 'https' }
    r = requests.get('http://localhost:8081/{}'.format(path), allow_redirects=False, headers=headers)

    assert r.status_code == code, 'got code {} but expected {} fetching {!r} with source IP {}'.format(r.status_code, code, path, source_ip)
    assert r.headers['Some-Always-Header'] == 'some always "value"'
    assert not some_header or r.headers['Some-Header'] == 'some "value"'


# regression test for #63270:
# ensure cache isn't shared across different hostnames
def test_cache_keying():
    abc_net1 = requests.get('http://localhost:8081/cache', headers={'Host': 'abc.net', 'X-Forwarded-Proto': 'https', 'X-Forwarded-For': '10.0.0.3'})
    assert abc_net1.headers['X-Cache'] == 'MISS'

    # same schema, same host, same uri and same method → cached
    abc_net2 = requests.get('http://localhost:8081/cache', headers={'Host': 'abc.net', 'X-Forwarded-Proto': 'https', 'X-Forwarded-For': '10.0.0.3'})
    assert abc_net2.headers['X-Cache'] == 'HIT'
    assert abc_net1.text == abc_net2.text

    # different schema → not cached
    http = requests.get('http://localhost:8081/cache', headers={'Host': 'abc.net', 'X-Forwarded-Proto': 'http', 'X-Forwarded-For': '10.0.0.3'})
    assert http.headers['X-Cache'] == 'MISS'
    assert abc_net1.text != http.text

    # different host → not cached
    xyz_net = requests.get('http://localhost:8081/cache', headers={'Host': 'xyz.net', 'X-Forwarded-Proto': 'https', 'X-Forwarded-For': '10.0.0.3'})
    assert xyz_net.headers['X-Cache'] == 'MISS'
    assert abc_net1.text != xyz_net.text

    # differentt method - not cached
    abc_net2 = requests.head('http://localhost:8081/cache', headers={'Host': 'abc.net', 'X-Forwarded-Proto': 'https', 'X-Forwarded-For': '10.0.0.3'})
    assert abc_net2.headers['X-Cache'] == 'MISS'
    assert abc_net1.text != abc_net2.text


def test_no_head_rewrite_to_get():
    get = requests.get('http://localhost:8081/method', headers={'Host': 'abc.net', 'X-Forwarded-Proto': 'https', 'X-Forwarded-For': '10.0.0.3'})
    print(get.headers)
    assert get.headers['X-Method-Received'] == 'GET'

    head = requests.head('http://localhost:8081/method', headers={'Host': 'abc.net', 'X-Forwarded-Proto': 'https', 'X-Forwarded-For': '10.0.0.3'})
    assert head.headers['X-Method-Received'] == 'HEAD'


# Test if status-tocco-nginx returns 200 even if Nice isn't reachable
def test_unreachable():
    assert_response('status-tocco-nginx', 200, some_header=True)
    assert_response('bad-gateway', 502, some_header=False)


# Verify forwarding to upstream works and headers are set
def test_proxying():
    assert_response('200', 200, some_header=True)
    assert_response('404', 404, some_header=False)
    assert_response('500', 500, some_header=False)


# verify default timeout of 60s is not in effect
def test_timeout():
    print('Testing 4 minute timeout. Be patient.')
    assert_response('time=240', 200,  some_header=True)


def test_websocket():
    # Ensure the 'Connection' and 'Upgrade' HTTP headers are properly forwarded
    # to the web server. The implementation for the /websocket route does the
    # actual testing.
    resp = requests.get(
        'http://localhost:8081/websocket',
        allow_redirects=False,
        headers={
            'Connection': 'Upgrade',
            'Upgrade': 'WebSocket',
            'X-Forwarded-For': '10.0.0.3',
            'X-Forwarded-Proto': 'https',
        }
    )
    resp.raise_for_status()


def test_no_websocket():
    # Neither the 'Connection' nor the 'Upgrade' header should be sent
    # to upstreams. The web server tests this.
    resp = requests.get(
        'http://localhost:8081/no-websocket',
        allow_redirects=False,
        headers = {
            'X-Forwarded-For': '10.0.0.3',
            'X-Forwarded-Proto': 'https',
        }
    )
    resp.raise_for_status()

def test_security_txt():
    resp = requests.get('http://localhost:8081/.well-known/security.txt')
    resp.raise_for_status()
    assert 'Expires: 20' in resp.text

    resp = requests.get('http://localhost:8081/security.txt')
    resp.raise_for_status()
    assert 'Expires: 20' in resp.text

    resp = requests.get('http://localhost:8081/security.txt', allow_redirects=False)
    resp.raise_for_status()
    assert resp.headers['Location'] == '/.well-known/security.txt'

class mock:
    def __init__(self):
        self.mock = None

    def __enter__(self):
        self.mock = Process(target=nice_mock.run)
        self.mock.start()

        # wait for mock to become ready
        while True:
            try:
                requests.get('http://localhost:8080/200')
            except requests.exceptions.ConnectionError:
                print('waiting for mock …')
                time.sleep(0.5)
            else:
                break

    def __exit__(self, a, b, c):
        if self.mock is not None:
            self.mock.terminate()


def main():
    test_security_txt()
    test_unreachable()

    with mock():
        test_cache_keying()
        test_no_head_rewrite_to_get()
        test_proxying()
        test_timeout()
        test_websocket()
        test_no_websocket()

if __name__ == '__main__':
    main()
